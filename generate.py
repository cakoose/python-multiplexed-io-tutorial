# Used to randomly generate a bunch of pages and links.  Save the output of this
# program into "web.json", which is then used by "servers.js".

import base64
import json
import os
import random
import sys

PAGES_TO_GENERATE = 30

def create_page(links):
    return dict(
        password=base64.b64encode(os.urandom(3)),
        password_delay=random.randint(0,2000),
        links=links,
        links_delay=random.randint(0,2000),
    )

def main():
    words = open(sys.argv[1], 'rb').read().splitlines()

    pages = {"": create_page([])}
    page_list = [""]

    for new_page in random.sample(words, PAGES_TO_GENERATE):
        for _ in xrange(random.randint(1, min(len(pages), 3))):
            page_from = random.choice(page_list)
            pages[page_from]['links'].append(new_page)

        pages[new_page] = create_page(
            [random.choice(page_list) for _ in xrange(random.randint(0, min(len(pages), 2)))])
        page_list.append(new_page)

    print json.dumps(pages, indent=4)

if __name__ == '__main__':
    main()
