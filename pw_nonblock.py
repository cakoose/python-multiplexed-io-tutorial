import json
import sys

import comm
import comm_nonblock

def main():
    prog_name, args = sys.argv[0], sys.argv[1:]
    
    if len(args) != 1:
        sys.stderr.write("Expecting a single argument, got {}.\n".format(len(args)))
        sys.exit(1)

    page_name = args[0]

    sock = comm_nonblock.connect(comm.PASSWORD_SERVER)

    req = json.dumps(dict(page=page_name))
    while True:
        # Non-blocking send() tries sending as much as it can.  If was able to send
        # everything, it returns None.  If there's more to send, it returns the
        # remaining data.
        req = comm_nonblock.send(sock, req)
        if req is None:
            break

    # We'll call non-blocking recv() multiple times.  Each time it will only give us the
    # data that was available.  Collect each fragment into 'data_list'.
    data_list = []
    while True:
        data_part = comm_nonblock.recv(sock)
        if data_part is None:
            break
        if len(data_part) > 0:
            data_list.append(data_part)
    data = b''.join(data_list)

    resp = json.loads(data)
    print resp['ok']

if __name__ == '__main__':
    main()
