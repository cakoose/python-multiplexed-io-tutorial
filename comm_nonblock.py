from __future__ import absolute_import, unicode_literals

import errno
import socket

# Artificially limit how much we receive before going back to select() again.  Since our
# actual reads/writes are small, without this we wouldn't exercise the partial data
# code paths.
CHUNK = 2

def connect(server):
    """Returns a socket."""
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.setblocking(0)
    r = sock.connect_ex(server)
    assert r == errno.EINPROGRESS, (r, sock, server)
    return sock

def send(sock, data):
    """
    Send as much of 'data' as we can send without blocking.  Return the unsent remainder.
    If everything was sent, return None.
    """
    assert isinstance(data, str)
    assert sock.gettimeout() == 0.0, "Expecting non-blocking socket, got blocking one."
    if len(data) == 0:
        return None
    try:
        n = sock.send(data[:CHUNK])
    except socket.error as e:
        # If they're not using select(), they might try to send too early.
        if e[0] == errno.ENOTCONN:
            return data
        raise
    assert n > 0, "Unexpected.  Thought it would raise EWOULDBLOCK in this case."
    data = data[n:]
    if len(data) == 0:
        sock.shutdown(socket.SHUT_WR)
        return None
    return data

def recv(sock):
    """
    Read any data that is available.  If we hit the end of the stream, return None.
    """
    assert sock.gettimeout() == 0.0, "Expecting non-blocking socket, got blocking one."
    try:
        buf = sock.recv(CHUNK)
    except socket.error as e:
        if e[0] in (errno.EAGAIN, errno.EWOULDBLOCK):
            return b''
        raise
    if len(buf) == 0:
        sock.close()
        return None
    return buf
