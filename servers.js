"use strict";

var net = require("net")
var fs = require("fs")

var web = JSON.parse(fs.readFileSync("web.json"))

function getJsonReq(socket, f) {
  var content = '';
  function respond(msg) {
    socket.write(JSON.stringify(msg))
    socket.end();
    socket.destroy()
  }
  socket.on('data', function(buffer) {
    content += buffer.toString();
  });
  socket.on('end', function() {
    try {
      var parsed = JSON.parse(content)
    } catch (err) {
      return respond({error: 'not_json'})
    }
    f(parsed, respond)
  });
  socket.on('error', function() {
    socket.end();
    socket.destroy();
  });
}

// Password server.
net.createServer({allowHalfOpen: true}, function (socket) {
  getJsonReq(socket, function (req, respond) {
    console.log("password req " + JSON.stringify(req))
    if (web.hasOwnProperty(req.page)) {
      var page = web[req.page]
      setTimeout(function () {
        return respond({ok: page.password})
      }, page.password_delay)
    } else {
      return respond({error: 'not_found'})
    }
  })
}).listen({
  port: 8098,
  exclusive: true,
});

// Link server.
net.createServer({allowHalfOpen: true}, function (socket) {
  getJsonReq(socket, function(req, respond) {
    console.log("link req " + JSON.stringify(req))
    if (web.hasOwnProperty(req.page)) {
      var page = web[req.page]
      if (req.password === page.password) {
        setTimeout(function () {
          return respond({ok: page.links})
        }, page.links_delay)
      } else {
        return respond({error: 'wrong_password'})
      }
    } else {
      return respond({error: 'not_found'})
    }
  })
}).listen({
  port: 8099,
  exclusive: true,
});

// Just respond with "hello world".
var helloResponse = "Hello, world!"
net.createServer({allowHalfOpen: true}, function (socket) {
  console.log("got connection")
  var die = false;
  socket.on('error', function() {
    console.log("connection error")
    die = true
  })
  var i = 0
  function f() {
    if (!die && i < helloResponse.length) {
      socket.write(helloResponse.substring(i, i+2))
      i += 2
      setTimeout(f, 300)
    } else {
      socket.end()
      socket.destroy()
    }
  }
  f()
}).listen({
  port: 8097,
  exclusive: true,
});

console.log("Started servers: password, link, hello");
