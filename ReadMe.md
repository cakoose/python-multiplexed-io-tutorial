# Python Multiplexed I/O Tutorial

## Prerequisites

1. Python 2.7.x (run `python --version` to check)
2. Node.js 5 or later (run `node --version` to check)

## Setup

1. `git clone https://bitbucket.org/cakoose/python-multiplexed-io-tutorial`
2. `mkdir solutions`
3. `cd solutions`
4. In the "solutions/" folder, create a file called "test.py":

        import comm
        print comm.PASSWORD_SERVER

5. `PYTHONPATH=../python-multiplexed-io-tutorial python test.py`

If you see the output `(u'localhost', 8098)`, then setup is complete.

Put all your code in the "solutions/" folder and follow the example above to set `PYTHONPATH` correctly when running your code.

## Part 1: A web crawler

Imagine a web service that has a bunch of "pages" that link to each other.  The root page is named "" (empty string).  Your program should start from the root page and find all the pages that are reachable (via one or more links) from the root.

The web service is implemented by a set of servers.  To start them, run this in a separate terminal window:

    cd python-multiplexed-io-tutorial
    node servers.js

One server is the password server.  Send it a page name and it will respond with the page's password:

    import comm
    import comm_block

    page_name = ...
    resp = comm_block.rpc(comm.PASSWORD_SERVER, dict(page=page_name))
    if 'ok' not in resp:
        raise Exception("password server said: {!r}".format(resp))
    page_password = resp['ok']
    print "password for {!r} is {!r}".format(page_name, page_password)

The other is the link server.  Send it a page name and password and it will respond with a list of page names that are linked from the given page.

    page_name = ...
    page_password = ...
    resp = comm_block.rpc(comm.LINK_SERVER,
                          dict(page=page_name, password=page_password))
    if 'ok' not in resp:
        raise Exception("link server said: {!r}".format(resp))
    links = resp['ok']
    print "linked from {!r}".format(page_name)
    for linked_page_name in links:
        print "- {!r}".format(linked_page_name)

Your task: write "crawl.py", a crawler that communicates with the servers and prints out the names of all the pages reachable from the root page.  At the end, print the total number of pages you've found.

For example:

    $ PYTHONPATH=../python-multiplexed-io-tutorial python crawl.py
    thing
    number
    point
    ...
    count: 31

## Part 2: Threads.

Instead of doing everything sequentially, you can use multiple threads to do many things concurrently.

To use a thread, you need the 'threading' module:

    import threading

To run a function in a separate thread:

    def my_function(a, b, mode=1):
        ...
        ...

    # Start a thread.
    thread = threading.Thread(target=my_function, args=('a', 'b'), kwargs=dict(mode=13))
    thread.start()

To wait for a thread to finish:

    thread.join()  # Just waits until the thread function, then returns.

Write "crawl_threads.py", a crawler that uses threads to fetch more things simultaneously.  When you see a link, start a new thread to fetch that page's password and subsequent links.  At the end, print the total number of pages you've found.

## Part 3: Non-blocking I/O.

There can be memory and CPU overheads to using many threads.  To avoid this, you can use non-blocking I/O to handle multiple sockets with a single thread.

The I/O operations you've performed until now are "blocking" -- if there's no data available, the OS puts your process to sleep and the function call just hangs.  When data is available, the OS wakes your process up and the function call returns the data.  Non-blocking operations are different -- if there's no data available, the function call returns immediately, allowing you to go do other stuff (like check for data on other sockets).

There's an example program called "pw_nonblock.py" that gets the password for a single page using non-blocking I/O.

To try it out:

1. Peek into "web.json" to get the name of some page.
2. Run "pw_nonblock.py <page-name>".

### Part 3a

"pw_nonblock.py" is kind of silly.  Since you're only reading/writing a single socket at a time, there's no reason to use non-blocking I/O.

The real advantage of non-blocking I/O is that it lets you read/write multiple sockets with a single thread.  Write "list_nonblock.py" that is similar to "pw_nonblock.py", except that it gets the password for a bunch of pages simultaneously.  Your program should:

1. Accept a list of page names as command-line arguments (`sys.argv[1:]`).
2. Open multiple sockets to the password server, one per page name.
3. Write the JSON requests to all the sockets simultaneously.
4. Once all the writes are complete, reads the JSON responses from all the sockets simultaneously.
5. Print out all the "<page-name>: <password>" pairs as you get the responses.

Example:
    
    $ PYTHONPATH=../python-multiplexed-io-tutorial python list_nonblock.py thing number point
    number: UY42
    thing: c8ng
    point: ZQML

## Part 3b: Use select() to avoid "busy-waiting".

Up until now, our non-blocking code just keeps looping until one of the sockets is ready to be read/written.  This is called "busy-waiting".  Busy-waiting is bad because we're using CPU time on a loop that isn't really doing anything.

The select() function lets you avoid using CPU time when there's nothing to do.  How it works:

1. Let's say you have a bunch of sockets you want to read/write from.
2. Pass that list of sockets to select().
3. If non of them are ready to read/write, your process will be put to "sleep", which stops using CPU time.
4. When any of the sockets become available to read/write, your process is woken up and select() returns.
5. The return value is the list of sockets that are ready to be read/written.

Here's an example of using select() to read from a bunch of sockets simultaneously:

    import select

    sockets_to_read = [...]

    rlist, _, xlist = select.select(sockets_to_read, (), sockets_to_read)
    assert len(xlist) == 0, "Got exceptions on sockets."

    for sock in rlist:
        # For each socket that has some data available, read from it.
        data_part = comm_nonblock.read(sock)
        print "Got a little data from {!r}: {!r}".format(sock, data_part)

Copy "list_nonblock.py" (from Part 3) to "list_select.py", then modify it to use select() so we don't waste CPU time when there's nothing to do.

Once you're done, use the `time` utility to see how much CPU time you're actually using:

    time PYTHONPATH=... python list_nonblock.py ...
    time PYTHONPATH=... python list_select.py ...

The `time` utility will report three values:
- "real": How much time elapsed in the real world while your program ran.
- "user": How much time your program was actually running on a CPU (as opposed to sleeping).
- "sys": How much time the OS kernel was running on a CPU doing work for your program (e.g. splitting your network requests into network packets, sending them to your network hardware, etc).

Add up the "user" and "sys" values to determine how much total CPU time your program used.

### Part 3c: Performing input and output together.

Your "list_select.py" first does all the writes, then does all the reads.  This means it waits for all the writes to finish before doing the first read.  It would be more efficient to start the reads as soon as possible.

Copy your "list_select.py" (from Part 4) to "list_select_rw.py", then modify it to start reading from a socket as soon as it is available.

### Part 3d: Back to web crawler.

Use non-blocking I/O and select() to make a web crawler that does work concurrently without creating additional threads.

## Part 4: Event loop and callbacks.

The select()-based crawler is more difficult to write than the multi-threaded crawler.  One issue is that all the logic is in one place.  With threads, you can just start a thread and have it run arbitrary code.  With select(), you need all that code tied in to your main select() loop.

To fix that, we can write a generic "event loop" that handles the select() stuff and then calls out to functions you provide.

Here's the EventLoop API:

    class EventLoop(object):
        def on_read_ready(self, sock, f):
            """When 'sock' is ready to be read, call the function 'f'"""
        def on_write_ready(self, sock, f):
            """When 'sock' is ready to be written, call the function 'f'"""
        def run(self):
            """As long as there are sockets people are waiting on, keep
               looping and processing the callbacks."""

This is how I might use EventLoop to read from a socket:

    import comm
    import comm_nonblock
    import ev

    event_loop = ev.EventLoop()

    def connect():
        print "Connecting..."
        sock = comm_nonblock.connect(comm.HELLO_SERVER)
        event_loop.on_read_ready(sock, lambda: read1(sock))

    def read1(sock):
        data1 = comm_nonblock.recv(sock)
        print "Got data1: {!r}".format(data1)
        event_loop.on_read_ready(sock, lambda: read2(sock, data1))

    def read2(sock, data1):
        data2 = comm_nonblock.recv(sock)
        print "Got data2: {!r}".format(data2)
        event_loop.on_read_ready(sock, lambda: read3(sock, data1, data2))

    def read3(sock, data1, data2):
        data3 = comm_nonblock.recv(sock)
        print "Got data3: {!r}".format(data3)
        print "DONE: {!r}".format((data1, data2, data3))

    # Start the first fragment.
    connect()

    print "EventLoop starting..."
    event_loop.run()
    print "EventLoop finished."

## Part 4a

1. Write the EventLoop class (in a file called "ev.py").
2. Copy the example above into a "dump_cb_three.py".
3. Run "node servers.js" to start the hello server.

(When you connect to that server, it will send back "Hello, world!" and then close the socket.  To see it work, try netcat: "nc localhost 8097".)

## Part 4b

What's a little annoying about "dump_cb_three.py" is each subsequent function needs to have a bunch of data from the previous function (like 'sock', 'data1', 'data2').  One way to avoid this is to take advantage of closures.

Instead of defining read1, read2, and read3 as top-level functions.  Write "dump_cb_nested.py", where the functions are nested inside each other, so each subsequent function has access to all the variables of the outer one.

(Aside: Node.js is based on an event loop and callbacks.  Javascript is a little nicer than Python for this because you can define anonymous/lambda functions with multiple statements.  In Python, lambdas can only be a single expression, so you're forced to use nested functions instead, which make things slightly messier.)

## Part 4c

Write "dump_cb_full.py", which reads all the data from the socket -- and not just the first three chunks.

    Connecting...
    EventLoop starting...
    Got data: 'He'
    Got data: 'll'
    Got data: 'o,'
    Got data: ' w'
    Got data: 'or'
    Got data: 'ld'
    Got data: '!'
    DONE: 'Hello, world!'
    EventLoop finished.

Does it work if you start two concurrent tasks at the beginning?

## Part 4d

Create a helper function to receive data on a socket.  Put it in "comm_ev.py".

    recv(event_loop, sock, f):
        """Read from 'sock' until there's no more data to read.  When finished
           call f(data)."""

Create "dump_cb_helper.py", a modified version of "dump_cb_full.py" that uses this helper function.

## Part 4e

Create a helper function to send data on a socket.  Put it in "comm_ev.py".

    recv(event_loop, sock, f):
        """Read from 'sock' until there's no more data to read.  When finished
           call f(data)."""

With these helper functions, write "list_cb.py", which should work like "list_select_rw.py" -- given a list of page names on the command line, get the passwords for all those pages.

## Part 4e

Write "crawl_cb.py", a crawler that uses the event loop and callbacks.

## Part 5: Generators.

The annoying thing with callbacks is that you have to split your code up every time you want to do I/O.

Python has a feature called "generators" where you can write normal code and Python automatically splits it up.

    def f():
        print "About to yield 1"
        yield 1
        print "About to yield 'hello'"
        yield 'hello'
        print 'DONE'

Python converts this code into something that can be paused/resumed.  Every time you call next() on the object, it will run until the next 'yield'.

    >>> gen = f()
    >>> gen.next()     # prints "About to yield 1", then returns 1
    >>> gen.next()     # prints "About to yield 'hello'", then returns 'hello'
    >>> gen.next()     # prints "DONE", then raises StopIteration

We can use this for our purposes by yielding every time we're waiting to receive data on the socket.  The event loop can do the waiting for us, resuming our generator (by calling next()) when data is available to read.

## Part 5a

1. Write a new event loop class, specifically for generators, in "gen.py".
2. Write "dump_gen_three.py", which should look like this:

        import comm
        import comm_nonblock
        import gen

        def main():
            event_loop = gen.EventLoop()
            event_loop.run(dumper())

        def dumper():
            sock = comm_nonblock.connect(comm.HELLO_SERVER)

            yield sock  # tells the event loop "resume me when there's data to recv()"
            data1 = comm_nonblock.recv(sock)
            print "Got data1: {!r}".format(data1)

            yield sock
            data2 = comm_nonblock.recv(sock)
            print "Got data2: {!r}".format(data2)

            yield sock
            data3 = comm_nonblock.recv(sock)
            print "Got data3: {!r}".format(data3)

        if __name__ == '__main__':
            main()

## Part 5b

The point of all this was to be able to run things concurrently.  Change EventLoop.run() to take a list of generators and run all of them concurrently.  For example:

    event_loop.run(dumper(), dumper())

This should cause your program to make two connections to the hello server and read from those connections concurrently.

## Part 5c

Write "dump_gen_full.py" that reads everything from the socket, not just the first three chunks.

## Part 5d

Right now, a function "yields" a socket to say "resume me when it's ready to read".  But you also need a way to say "resume me when it's ready to write".

1. Come up with a way to signal both of these cases via "yield".
2. Change your event loop to handle both cases.
3. Write "list_gen.py".  It should take list of page names as command-line arguments and get the passwords for all of them.

    $ PYTHONPATH=../python-multiplexed-io-tutorial python list_gen.py thing number point
    number: UY42
    thing: c8ng
    point: ZQML

## Part 5e

Write "list_gen_helper.py", which does the `send()` loop using a helper function.  How do you call that function?

Now use a helper function for the `recv()` loop.  This is a little trickier than `send()`, because `recv()` needs to pass a value back to its caller somehow.

## Part 5f

Your `send()` and `recv()` helpers are themselves generators.  When you call them, you need to have a loop to re-`yield` all of the values back to the event loop.  This is inconvenient.  It would be better if you could just do:

    yield send(...)

This yields the sub-generator itself directly to the event loop.  At that point, the event loop is should:

1. Put the current generator on hold.
2. Run the sub-generator.
3. When the sub-generator is complete, resume running the original generator.

Modify your event loop to make this work, then write "list_gen_easy.py" to take advantage of this new feature.

## Part 5g

`yield` pauses a generator and returns a value.  Did you know that, when resuming a generator, you can send a value to it?

    def f():
        print "About to yield 1"
        r = yield 1
        print "Got back {!r}".format(r)
        r = yield 'hello'
        print "Got back {!r}".format(r)

Python converts this code into something that can be paused/resumed.  Every time you call `next()` on the object, it will execute until the next `yield`.

    >>> gen = f()
    >>> gen.send(None) # prints "About to yield 1", then returns 1
    >>> gen.send(2)    # prints "Got back 2", then returns 'hello'
    >>> gen.send(3)    # prints "Got back 3", then raises StopIteration

Using this feature, modify the event loop to make it easier for a helper function to return a value to its caller.  This would allow `recv()` return "return" a value:

    data = yield recv(...)

Write "list_gen_return.py" to take advantage of this.

## Part 5h

Make sure your event loop can handle multiple levels of helper functions.

Write "list_gen_rpc.py" that has an two additional helper functions:

1. `rpc()`: connects to the given server, then calls your helper `send()` and `recv()`
2. `rpc_json()`: calls `rpc()` and takes care of JSON encoding/decoding your request/response.

## Part 5i

Currently, you can start multiple concurrent tasks by passing multiple things into `run()`.  But what if you're in a task, you can't "spawn" more tasks.

For example, lets say we write "list_gen_spawn.py", that does this:

    event_loop.run(get_all(page_names))

    def get_all(page_names):
        for page_name in page_names:
            ???  # spawn a task to get the password for the given page

Modify your event loop to allow code like this to work.

## Part 5j

Let's say we want to print "DONE" once all the "get page password" tasks are complete.  We could wait until `EventLoop.run()` returns, but that's not a great solution -- this means you have to wait for ALL tasks to finish, not just the ones you care about.

In multi-threaded programming, `join()` gives you the ability to wait for another thread to finish.  Modify the event loop to allow waiting for another spawned task to finish.  Write "list_gen_join.py" to take advantage of this and print "DONE" when all threads are complete.

## Part 5k

Write a generator-based web crawler.  Don't forget to print the total page count at the end.
