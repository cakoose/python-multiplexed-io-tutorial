from __future__ import absolute_import, unicode_literals

import json
import socket

def rpc_json(server, req):
    resp = rpc(server, json.dumps(req))
    try:
        return json.loads(resp)
    except ValueError as e:
        raise ValueError("not valid JSON: {!r}: {}".format(resp, e))

def rpc(server, req):
    sock = connect(server)
    try:
        send(sock, req)
        sock.shutdown(socket.SHUT_WR)
        resp = recv(sock)
    finally:
        sock.close()
    return resp

def connect(server):
    """Returns a socket."""
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect(server)
    return sock

def send(sock, data):
    assert sock.gettimeout() is None, "Expecting a blocking socket, got a non-blocking one."
    while len(data) > 0:
        n = sock.send(data)
        assert n > 0, "Are you sure the socket is blocking?"
        data = data[n:]

def recv(sock):
    assert sock.gettimeout() is None, "Expecting a blocking socket, got a non-blocking one."
    bufs = []
    while True:
        buf = sock.recv(4096)
        if len(buf) == 0:
            break
        bufs.append(buf)
    return b''.join(bufs)
